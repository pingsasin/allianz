package pattararittigul.sasin.allianzassignment

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import pattararittigul.sasin.allianzassignment.database.AppDatabase
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

@RunWith(AndroidJUnit4ClassRunner::class)
class CountryDatabaseTest {

    private var countryDao: CountryDao? = null

    private val firstCountry = CountryEntity(alpha3Code = "1ST", name = "Thailand", favorite = 1)
    private val secondCountry = CountryEntity(alpha3Code = "2ND", name = "England", favorite = 0)
    private val thirdCountry = CountryEntity(alpha3Code = "3RD", name = "Japan", favorite = 1)
    private val mockCountryList: List<CountryEntity> = listOf(firstCountry, secondCountry, thirdCountry)


    @Before
    fun setup() {
        countryDao = AppDatabase.getInstance(InstrumentationRegistry.getInstrumentation().targetContext).countryDao()
        countryDao?.insertAllCountries(mockCountryList)
    }

    @After
    fun teardown() {
        countryDao?.deleteAllCountries()
    }

    @Test
    fun should_Insert_All_Countries() {
        countryDao?.insertAllCountries(mockCountryList)
        assert(countryDao?.getCountriesCount() == Single.just(3))
    }

    @Test
    fun should_Delete_All_Countries() {
        countryDao?.deleteAllCountries()
        assert(countryDao?.getCountriesCount() == Single.just(0))
    }
}