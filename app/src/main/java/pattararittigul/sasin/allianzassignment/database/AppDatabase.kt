package pattararittigul.sasin.allianzassignment.database

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pattararittigul.sasin.allianzassignment.database.converter.ListOfLanguageConverter
import pattararittigul.sasin.allianzassignment.database.converter.ListOfStringConverter
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

@VisibleForTesting
const val DATABASE_NAME = "android-room.db"

@Database(
    entities = [CountryEntity::class],
    version = 4,
    exportSchema = false
)
@TypeConverters(ListOfStringConverter::class, ListOfLanguageConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun countryDao(): CountryDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, DATABASE_NAME
            ).fallbackToDestructiveMigration()
                .build()
    }
}