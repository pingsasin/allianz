package pattararittigul.sasin.allianzassignment.database

import android.content.Context
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao
import pattararittigul.sasin.allianzassignment.viewmodel.ViewModelFactory

object Injection {

    private fun provideCountryDataSource(context: Context): CountryDao {
        val database = AppDatabase.getInstance(context)
        return database.countryDao()
    }

    fun provideViewModelFactory(context: Context): ViewModelFactory {
        val dataSource = provideCountryDataSource(context)
        return ViewModelFactory(dataSource)
    }


}