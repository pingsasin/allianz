package pattararittigul.sasin.allianzassignment.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

class ListOfLanguageConverter {

    companion object {
        @TypeConverter
        @JvmStatic
        fun fromLanguageList(list: MutableList<CountryEntity.Language>): String {
            val gSon = Gson()
            return gSon.toJson(list)
        }

        @TypeConverter
        @JvmStatic
        fun toLangugeList(value: String): MutableList<CountryEntity.Language> {
            val listType = object : TypeToken<ArrayList<CountryEntity.Language>>() {
            }.type
            return Gson().fromJson(value, listType)
        }
    }
}