package pattararittigul.sasin.allianzassignment.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ListOfStringConverter {
    companion object {
        @TypeConverter @JvmStatic
        fun fromString(value: String): MutableList<String> {
            val listType = object : TypeToken<ArrayList<String>>() {
            }.type
            return Gson().fromJson(value, listType)
        }

        @TypeConverter @JvmStatic
        fun toString(list: MutableList<String>): String {
            val gSon = Gson()
            return gSon.toJson(list)
        }
    }
}