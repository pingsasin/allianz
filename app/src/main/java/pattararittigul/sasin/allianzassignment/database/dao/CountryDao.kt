package pattararittigul.sasin.allianzassignment.database.dao

import androidx.room.*
import io.reactivex.Maybe
import io.reactivex.Single
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

@Dao
interface CountryDao {

    @Query("SELECT * FROM countryEntity")
    fun loadAllCountries(): Maybe<List<CountryEntity>>

    @Query("SELECT * FROM countryEntity WHERE favorite = 1")
    fun loadFavoriteCountries(): Maybe<List<CountryEntity>>

    @Query("SELECT * FROM countryEntity WHERE alpha_3_code = :alpha3Code")
    fun loadCountryById(alpha3Code: Int): Single<CountryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCountries(countries: List<CountryEntity>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCountry(country: CountryEntity)

    @Query("DELETE FROM countryEntity")
    fun deleteAllCountries()

    @Query("SELECT COUNT(*) FROM countryEntity")
    fun getCountriesCount(): Single<Int>

    @Query("UPDATE countryEntity SET favorite = 0")
    fun removeAllFavouriteCountries()
}