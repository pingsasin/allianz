package pattararittigul.sasin.allianzassignment.database.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "countryEntity")
data class CountryEntity(
    @ColumnInfo(name = "name")
    var name: String? = "",
    @Ignore
    var topLevelDomain: List<String?>? = listOf(),
    @ColumnInfo(name = "alpha_2_code")
    var alpha2Code: String? = "",
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "alpha_3_code")
    var alpha3Code: String = "",
    @ColumnInfo(name = "calling_codes")
    var callingCodes: MutableList<String>? = mutableListOf(),
    @ColumnInfo(name = "capital")
    var capital: String? = "",
    @ColumnInfo(name = "language")
    var languages: MutableList<Language>? = mutableListOf(),
    @Ignore
    var altSpellings: List<String?>? = listOf(),
    @ColumnInfo(name = "region")
    var region: String? = "",
    @ColumnInfo(name = "sub_region")
    var subregion: String? = "",
    @ColumnInfo(name = "population")
    var population: Int? = 0,
    @Ignore
    var latlng: List<Double?>? = listOf(),
    @Ignore
    var demonym: String? = "",
    @Ignore
    var area: Double? = 0.0,
    @Ignore
    var timezones: List<String?>? = listOf(),
    @Ignore
    var borders: List<String?>? = listOf(),
    @ColumnInfo(name = "native_name")
    var nativeName: String? = "",
    @Ignore
    var numericCode: String? = "",
    @ColumnInfo(name = "flag")
    var flag: String? = "",
    @Ignore
    var cioc: String? = "",
    @ColumnInfo(name = "favorite")
    var favorite: Int? = 0
) : Parcelable {

    @Parcelize
    data class Language(
        @SerializedName("iso639_1")
        var iso6391: String? = "",
        @SerializedName("iso639_2")
        var iso6392: String? = "",
        var name: String? = "",
        var nativeName: String? = ""
    ) : Parcelable
}