package pattararittigul.sasin.allianzassignment.extension

import android.content.Context
import android.content.Intent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorInt
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

fun AppCompatActivity.replaceFragmentNow(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    this.supportFragmentManager.transactNow {
        replace(containerId, fragment)
    }
}

fun AppCompatActivity.hideKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(
            Context
                .INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

fun Fragment.hideKeyboard() {
    (activity as? AppCompatActivity)?.hideKeyboard()
}

fun Fragment.replaceFragmentNow(@IdRes containerId: Int, fragment: Fragment) {
    hideKeyboard()
    (activity as AppCompatActivity).replaceFragmentNow(containerId, fragment)
}


private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

private inline fun FragmentManager.transactNow(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commitNow()
}


infix fun AppCompatActivity.setStatusBarColor(@ColorInt color: Int) {
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = color
}

fun AppCompatActivity.setKeyboardAlwaysHidden() {
    this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}


inline fun <reified T : AppCompatActivity?> AppCompatActivity.toActivity() {
    val intent = Intent(this, T::class.java)
    hideKeyboard()
    this.startActivity(intent)
}

inline fun <reified T : AppCompatActivity?> Fragment.toActivity() {
    val intent = Intent(requireContext(), T::class.java)
    hideKeyboard()
    this.startActivity(intent)
}
