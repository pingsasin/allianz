package pattararittigul.sasin.allianzassignment.extension

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Fragment.errorAlertDialog(message: String = "Something went wrong. Please try again.") {
    AlertDialog.Builder(requireContext())
        .setTitle("Oops!")
        .setMessage(message)
        .setPositiveButton("Dismiss") { dialog, _ ->
            dialog.dismiss()
        }
        .also { it.show() }
}

fun Fragment.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    secondButton: String,
    eventSecond: DialogInterface.OnClickListener,
    cancelable: Boolean = false
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setNegativeButton(secondButton, eventSecond)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Activity.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    secondButton: String,
    cancelable: Boolean = false
) {
    AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setNegativeButton(secondButton) { dialog, _ -> dialog.dismiss() }
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    cancelable: Boolean
) {
    AlertDialog.Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Activity.displayDialog(
    title: String = "",
    message: String = "",
    firstButton: String,
    eventFirst: DialogInterface.OnClickListener,
    cancelable: Boolean
) {
    AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(firstButton, eventFirst)
        .setCancelable(cancelable)
        .also {
            it.show()
        }
}

fun Fragment.showLoading(): ProgressDialog =
    ProgressDialog.show(requireContext(), "", "Loading", true, false)

fun ProgressDialog?.hideLoading() = this?.let {
    if (it.isShowing) this.dismiss()
}


fun Context.toast(text: CharSequence): Toast? =
    Toast.makeText(this, text, Toast.LENGTH_SHORT).apply { show() }

fun Fragment.toast(text: CharSequence) = activity?.toast(text)