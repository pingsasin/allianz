package pattararittigul.sasin.allianzassignment.extension

inline val <reified T : Any> T.TAG get() = this::class.java.simpleName.toString()
