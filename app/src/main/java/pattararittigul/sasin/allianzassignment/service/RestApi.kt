package pattararittigul.sasin.allianzassignment.service

import io.reactivex.Observable
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity
import retrofit2.http.GET

interface RestApi {
    @GET("all")
    fun getCountries(): Observable<MutableList<CountryEntity>>
}