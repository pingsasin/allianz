package pattararittigul.sasin.allianzassignment.service

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class Retrofit {
    companion object {
        fun retrofit(): Retrofit {
            val url = "https://restcountries.eu/rest/v2/"
            val gSon = GsonBuilder()
                .setLenient()
                .create()
            return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .client(okHttpClient())
                .baseUrl(url)
                .build()
        }

        private fun okHttpClient(): OkHttpClient {
            val bodyLogging = HttpLoggingInterceptor()
            bodyLogging.level = HttpLoggingInterceptor.Level.BODY
            return OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .addNetworkInterceptor(bodyLogging)
                .build()
        }
    }
}
