package pattararittigul.sasin.allianzassignment.ui

import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_country_detail.*
import org.parceler.Parcels
import pattararittigul.sasin.allianzassignment.R
import pattararittigul.sasin.allianzassignment.database.Injection
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity
import pattararittigul.sasin.allianzassignment.extension.displayDialog
import pattararittigul.sasin.allianzassignment.util.SvgSoftwareLayerSetter
import pattararittigul.sasin.allianzassignment.viewmodel.CountryViewModel
import java.text.DecimalFormat


class CountryDetailActivity : AppCompatActivity() {

    private lateinit var requestBuilder: RequestBuilder<PictureDrawable>
    private lateinit var viewModel: CountryViewModel

    private val decimalFormatter by lazy {
        return@lazy DecimalFormat("#,###,###")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_detail)
        viewModel =
                ViewModelProviders
                    .of(this, Injection.provideViewModelFactory(this))
                    .get(CountryViewModel::class.java)
        getExtraFromIntent()
    }

    private fun getExtraFromIntent() {
        val country: CountryEntity? = Parcels.unwrap(intent.extras.getParcelable("COUNTRY"))
        val index: Int = intent.extras!!.getInt("INDEX")
        if (country != null) {
            init(country, index)
            handleSGVImage(country)
        }
    }

    private fun init(country: CountryEntity, index: Int) {
        val languageNames = mutableListOf<String>()
        tvName.text = country.name
        country.languages!!.forEach { languageNames.add(it.name!!) }
        capitalDes.text = country.capital
        languageDes.text = languageNames.joinToString()
        populationDes.text = decimalFormatter.format(country.population)
        callingCodeDes.text = country.callingCodes?.joinToString()
        buttonPhone.setOnClickListener {
            val code = country.callingCodes?.first()
            if (!code.isNullOrBlank()) {
                displayConfirmCallDialog(code!!)
            }
        }
        handleFavorite(country, index)
    }

    private fun handleSGVImage(country: CountryEntity) {
        val requestOptions =
            RequestOptions().error(R.drawable.place_holder_image_view)

        requestBuilder = Glide.with(this)
            .`as`(PictureDrawable::class.java)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .listener(SvgSoftwareLayerSetter())

        val uri = Uri.parse(country.flag)
        requestBuilder.load(uri).into(ivFlag)

    }

    private fun handleFavorite(country: CountryEntity, index: Int) {
        when (country.favorite) {
            1 -> checkboxFavorite.isChecked = true
            0 -> checkboxFavorite.isChecked = false
        }

        checkboxFavorite.setOnCheckedChangeListener { _, isChecked ->
            when {
                isChecked -> {
                    val updated = country.copy().apply { favorite = 1 }
                    updateCountry(updated, index)
                }
                else -> {
                    val updated = country.copy().apply { favorite = 0 }
                    updateCountry(updated, index)
                }
            }
        }
    }


    private fun updateCountry(country: CountryEntity, index: Int) {
        viewModel.updateCountry(country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onComplete = { beforeDestroy(country, index) })
    }

    private fun beforeDestroy(country: CountryEntity, index: Int) {
        val intent = Intent()
        val bundle = Bundle()
        bundle.putParcelable("COUNTRY", Parcels.wrap(country))
        bundle.putInt("INDEX", index)
        intent.putExtras(bundle)
    }

    private fun displayConfirmCallDialog(telNo: String) {
        displayDialog("Do you want to place a call?",
            "$telNo",
            "Call",
            DialogInterface.OnClickListener{dialog, _ ->
                toIntentDial(telNo)
                dialog.dismiss()
            },
            "Cancel",false)
    }

    private fun toIntentDial(telNo: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:+${Uri.encode(telNo.trim())}")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
