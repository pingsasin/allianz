package pattararittigul.sasin.allianzassignment.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_country_list.*
import org.parceler.Parcels
import pattararittigul.sasin.allianzassignment.R
import pattararittigul.sasin.allianzassignment.database.Injection
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity
import pattararittigul.sasin.allianzassignment.extension.hideKeyboard
import pattararittigul.sasin.allianzassignment.service.RestApi
import pattararittigul.sasin.allianzassignment.service.Retrofit
import pattararittigul.sasin.allianzassignment.viewmodel.CountryViewModel

class CountryListActivity : AppCompatActivity() {

    private var adapter: CountryListAdapter = CountryListAdapter(mutableListOf())
    private lateinit var viewModel: CountryViewModel
    private var displayState = DisplayState.STATE_ALL

    private var exitDelayCounter = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_list)
        viewModel =
            ViewModelProviders
                .of(this, Injection.provideViewModelFactory(this))
                .get(CountryViewModel::class.java)
        init()
    }

    private fun init() {

//        searchEditFrame.editText?.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?)  {
//                if (s.toString().isEmpty()) {
////                    displayState = DisplayState.STATE_ALL
////                    loadCountryList()
//                }
//            }
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
//            @SuppressLint("RestrictedApi")
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//            }
//        })
        initRecyclerView()
        fabFavorite.setOnClickListener {
            when (displayState) {
                DisplayState.STATE_ALL -> {
                    displayState = DisplayState.STATE_FAVORITE
                    fabFavorite.setImageDrawable(resources.getDrawable(R.drawable.ic_reply_all))
                    searchEditFrame.visibility = View.GONE
                }
                DisplayState.STATE_FAVORITE-> {
                    displayState = DisplayState.STATE_ALL
                    fabFavorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favorite_white))
                    searchEditFrame.visibility = View.VISIBLE
                }
                else -> {
                    displayState = DisplayState.STATE_ALL
                    fabFavorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favorite_white))
                    searchEditFrame.editText?.text?.clear()
                }
            }
            hideKeyboard()
            loadCountryList()
        }

        searchEditFrame.editText?.let {
            it.setOnEditorActionListener { _, actionId, event ->
                return@setOnEditorActionListener when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH -> {
                        displayState = DisplayState.STATE_SEARCH
                        fabFavorite.setImageDrawable(resources.getDrawable(R.drawable.ic_reply_all))
                        loadCountryList()
                        true
                    }
                    else -> false
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        fetchData()
    }

    private fun fetchData() {
        viewModel.getCountryCount()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                when (it > 0) {
                    true -> loadCountryList()
                    false -> executeService()
                }
            })
    }

    private fun executeService() {
        Retrofit.retrofit().create(RestApi::class.java).getCountries()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    viewModel.insertAllCountries(it).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy {
                            loadCountryList()
                        }
                })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val divider =
            DividerItemDecoration(recyclerViewCountries.context, layoutManager.orientation)
        recyclerViewCountries.layoutManager = layoutManager
        recyclerViewCountries.setHasFixedSize(true)
        recyclerViewCountries.itemAnimator = DefaultItemAnimator()
        recyclerViewCountries.addItemDecoration(divider, 0)
        recyclerViewCountries.adapter = adapter
        adapter.countryListListener(object : CountryListListener {
            override fun onCountryClick(country: CountryEntity, index: Int) {
                toDetailActivity(country, index)
                Toast.makeText(
                    this@CountryListActivity,
                    "${country.name} was clicked!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun toDetailActivity(countryEntity: CountryEntity, index: Int) {
        val intent = Intent(this, CountryDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable("COUNTRY", Parcels.wrap(countryEntity))
        bundle.putInt("INDEX", index)
        intent.putExtras(bundle)
        startActivityForResult(intent, 331)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 311) {
            val countryUpdate: CountryEntity =
                Parcels.unwrap(data?.extras?.getParcelable("COUNTRY"))
            val index: Int = data?.extras?.getInt("INDEX")!!

            adapter.updateCountry(index, countryUpdate)
        }
    }

    private fun loadCountryList() {
        when (displayState) {
            DisplayState.STATE_ALL -> viewModel.loadAllCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onSuccess = {
                    adapter.updateList(it.toMutableList())
                })
            DisplayState.STATE_SEARCH -> {
                viewModel.loadAllCountries()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onSuccess = {
                        adapter.updateList(
                            filterList(
                                searchEditFrame.editText?.text.toString(),
                                it.toMutableList()
                            )
                        )
                    })
            }
            else -> viewModel.loadFavoriteCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        adapter.updateList(it.toMutableList())
                    })
        }
    }

    private fun filterList(
        input: String,
        countryList: MutableList<CountryEntity>
    ): MutableList<CountryEntity> {
        return countryList.filter { it.name!!.contains(input.trim(),true) }.toMutableList()
    }

    enum class DisplayState { STATE_ALL, STATE_FAVORITE, STATE_SEARCH }

    override fun onBackPressed() {
        Handler().postDelayed({
            exitDelayCounter = false}
        ,2000)
        if (exitDelayCounter) {
            super.onBackPressed()
        } else {
            Toast.makeText(this, "click again to exit",Toast.LENGTH_SHORT).show()
            exitDelayCounter = true

        }
    }
}
