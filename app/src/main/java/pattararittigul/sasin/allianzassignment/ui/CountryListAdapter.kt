package pattararittigul.sasin.allianzassignment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.allianzassignment.R
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

class CountryListAdapter(
    private val countries: MutableList<CountryEntity>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val emptyState = 1
    private val dataState = 0
    private var listener: CountryListListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var holder: RecyclerView.ViewHolder? = null
        when (viewType) {
            dataState -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_country, parent, false)
                holder = CountryViewHolder(view)
            }
            emptyState -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_not_found_data, parent, false)
                holder = object : RecyclerView.ViewHolder(view) {}
            }
        }
        return holder!!
    }

    override fun getItemViewType(position: Int): Int {
        return if (countries.isEmpty()) emptyState else dataState
    }


    override fun getItemCount(): Int = if (countries.isEmpty()) 1 else countries.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CountryViewHolder) {
            val country = countries[position]
            holder.setupView(country)

            holder.itemView.setOnClickListener {
                listener?.onCountryClick(country, position)
            }
        }
    }

    fun clearList() {
        countries.clear()
        notifyDataSetChanged()
    }

    fun updateCountry(index: Int, country: CountryEntity) {
        countries[index] = country
        notifyItemChanged(index)
    }

    fun updateList(listUpdated: MutableList<CountryEntity>) {
        clearList()
        countries.addAll(listUpdated)
        notifyDataSetChanged()
    }

    fun countryListListener(callback: CountryListListener) {
        listener = callback
    }
}