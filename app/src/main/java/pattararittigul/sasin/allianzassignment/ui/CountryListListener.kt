package pattararittigul.sasin.allianzassignment.ui

import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

interface CountryListListener {
    fun onCountryClick(country: CountryEntity,index:Int)
}