package pattararittigul.sasin.allianzassignment.ui

import android.annotation.SuppressLint
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.list_item_country.view.*
import pattararittigul.sasin.allianzassignment.R
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity
import pattararittigul.sasin.allianzassignment.util.SvgSoftwareLayerSetter


class CountryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    lateinit var requestBuilder: RequestBuilder<PictureDrawable>

    @SuppressLint("SetTextI18n")
    fun setupView(country: CountryEntity) {
        view.textViewCountryName.text = country.name
        view.textViewDetails.text = "${country.subregion}, ${country.region}"
        view.imageViewFavorite.visibility = if (country.favorite == 1) View.VISIBLE else View.GONE
        handleSGVImage(country)
    }

    private fun handleSGVImage(country: CountryEntity) {
        val requestOptions =
            RequestOptions().placeholder(R.drawable.place_holder_image_view).error(R.drawable.place_holder_image_view)

        requestBuilder = Glide.with(view.context)
            .`as`(PictureDrawable::class.java)
            .apply(requestOptions)
            .transition(withCrossFade())
            .listener(SvgSoftwareLayerSetter())

        val uri = Uri.parse(country.flag)
        requestBuilder.load(uri).into(view.imageViewFlag)

    }
}