package pattararittigul.sasin.allianzassignment.util

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.ResourceDecoder
import com.bumptech.glide.load.engine.Resource
import com.caverock.androidsvg.SVG
import java.io.InputStream
import com.bumptech.glide.load.resource.SimpleResource

class SvgDecoder : ResourceDecoder<InputStream, SVG> {
    override fun handles(source: InputStream, options: Options): Boolean {
        return true
    }

    override fun decode(source: InputStream, width: Int, height: Int, options: Options): Resource<SVG>? {
        val svg = SVG.getFromInputStream(source)
        return SimpleResource<SVG>(svg)
    }
}