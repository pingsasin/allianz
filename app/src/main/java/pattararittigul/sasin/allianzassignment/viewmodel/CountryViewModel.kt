package pattararittigul.sasin.allianzassignment.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity

class CountryViewModel(private val dataSource: CountryDao) : ViewModel() {

    fun insertAllCountries(countries: List<CountryEntity>): Completable {
        return Completable.fromAction { dataSource.insertAllCountries(countries) }
    }

    fun updateCountry(country: CountryEntity): Completable {
        return Completable.fromAction { dataSource.updateCountry(country) }
    }

    fun loadAllCountries(): Maybe<List<CountryEntity>> {
        return dataSource.loadAllCountries()
    }

    fun loadFavoriteCountries(): Maybe<List<CountryEntity>> {
        return dataSource.loadFavoriteCountries()
    }

    fun loadCountryById(countryId: Int): Single<CountryEntity> {
        return dataSource.loadCountryById(countryId)
    }

    fun getCountryCount(): Single<Int> = dataSource.getCountriesCount()

    fun deleteAllCountries(): Completable {
        return Completable.fromAction { dataSource.deleteAllCountries() }
    }

    fun removeAllFavouriteCountries(): Completable {
        return Completable.fromAction { dataSource.removeAllFavouriteCountries() }
    }

}