package pattararittigul.sasin.allianzassignment.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val dataSource: CountryDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CountryViewModel::class.java)) {
            return CountryViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}