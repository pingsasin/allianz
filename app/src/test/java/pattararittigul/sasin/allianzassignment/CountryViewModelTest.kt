package pattararittigul.sasin.allianzassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Maybe
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import pattararittigul.sasin.allianzassignment.database.dao.CountryDao
import pattararittigul.sasin.allianzassignment.database.entity.CountryEntity
import pattararittigul.sasin.allianzassignment.viewmodel.CountryViewModel

class CountryViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var dataSource: CountryDao

    private lateinit var viewModel: CountryViewModel

    /**
     *  Mock Data for test view model
     */
    private val firstCountry = CountryEntity(alpha3Code = "1ST", name = "Thailand", favorite = 1)
    private val secondCountry = CountryEntity(alpha3Code = "2ND", name = "England", favorite = 0)
    private val thirdCountry = CountryEntity(alpha3Code = "3RD", name = "Japan", favorite = 1)
    private val mockCountryList: List<CountryEntity> = listOf(firstCountry, secondCountry, thirdCountry)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = CountryViewModel(dataSource)
    }

    @Test
    fun loadCountries_whenEntityIsEmpty() {
        `when`(dataSource.loadAllCountries()).thenReturn(Maybe.empty())
        viewModel.loadAllCountries().test().assertNoValues()
    }

    @Test
    fun loadCountries_whenDataSaved() {
        `when`(dataSource.loadAllCountries()).thenReturn(Maybe.just(mockCountryList))
        viewModel.loadAllCountries().test().assertValue(mockCountryList)
    }
    @Test
    fun loadFavoriteCountries_shouldReturn_subListThat_isFavoriteIs_1() {
        `when`(dataSource.loadFavoriteCountries()).thenReturn(Maybe.just(listOf(firstCountry, thirdCountry)))
        viewModel.loadFavoriteCountries().test().assertValue(listOf(firstCountry, thirdCountry))
    }
}